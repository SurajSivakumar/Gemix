#pragma once
#include"Util.h"
#include<GL\freeglut.h>
#include"Textures.h"

//Globals
extern int winX, winY;
int activeEditing=-1;
Util utilities;
Textures *textures;
using namespace std;

//Load raw images for buttons to memory
void loadRawsToMemory(Textures* tex, string resPath)
{
	textures = tex;
	string path = resPath;
	path.append("\\Res\\logo600x537.data");
	tex->LoadTexture(path, 600, 537, TEX_LOGO);
	path = resPath;
	path.append("\\Res\\fopen600x374.data");
	tex->LoadTexture(path, 600, 374, TEX_FOPEN);
	path = resPath;
	path.append("\\Res\\fsave297x299.data");
	tex->LoadTexture(path, 297, 299, TEX_FSAVE);
	path = resPath;
	path.append("\\Res\\crop256x256.data");
	tex->LoadTexture(path, 256, 256, TEX_CROP);
	path = resPath;
	path.append("\\Res\\rleft382x382.data");
	tex->LoadTexture(path, 382, 382, TEX_LROTATE);
	path = resPath;
	path.append("\\Res\\rright382x382.data");
	tex->LoadTexture(path, 382, 382, TEX_RROTATE);
	path = resPath;
	path.append("\\Res\\bright800x800.data");
	tex->LoadTexture(path, 800, 800, TEX_BRIGHT);
	path = resPath;
	path.append("\\Res\\contrast720x720.data");
	tex->LoadTexture(path, 720, 720, TEX_CONTRAST);
	path = resPath;
	path.append("\\Res\\saturate300x300.data");
	tex->LoadTexture(path, 300, 300, TEX_SATURATION);
	path = resPath;
	path.append("\\Res\\negative581x179.data");
	tex->LoadTexture(path, 581, 179, TEX_IMG_NEGATIVE);
	path = resPath;
	path.append("\\Res\\sepia145x113.data");
	tex->LoadTexture(path, 145,113, TEX_IMG_SEPIA);
	path = resPath;
	path.append("\\Res\\greyscale375x185.data");
	tex->LoadTexture(path, 375, 185, TEX_IMG_GRAYSCALE);
	path = resPath;
	path.append("\\Res\\coolshade442x558.data");
	tex->LoadTexture(path, 442, 558, TEX_IMG_COOLSHADE);
	path = resPath;
	path.append("\\Res\\warmsun478x579.data");
	tex->LoadTexture(path, 478, 579, TEX_IMG_WARMSUN);
	path = resPath;
	path.append("\\Res\\irregularsaturate512x592.data");
	tex->LoadTexture(path, 512, 592, TEX_IMG_IRRSATURATE);
	path = resPath;
	path.append("\\Res\\pixelblur512x711.data");
	tex->LoadTexture(path, 512, 711, TEX_IMG_BLUR);
}

//Set currently active editing context
void setActive(int active)
{
	activeEditing = active;
}

//Function to draw the left toolbar
void drawLToolBar(float X, float Y,bool fit)
{
	float box[][2] = { { X,Y },{ X + 7,Y },{ X + 7,93 },{ X,93 } };
	float plus[][2] = { {X + 3,Y + 30},{X + 4,Y + 30},{X + 4,Y + 31},{X + 5,Y + 31},{X + 5,Y + 32},{X + 4,Y + 32},{X + 4,Y + 33},{X + 3,Y + 33},{X + 3,Y + 32},{X + 2,Y + 32},{X + 2,Y + 31},{X + 3,Y + 31},{X+3,Y+30} };
	float phfill[][2] = { {X + 2,Y + 31},{X + 5,Y + 31},{X + 5,Y + 32},{X+2,Y+32} };
	float pvfill[][2] = { { X + 3,Y + 30 },{ X + 4,Y + 30 },{ X + 4,Y + 33 },{ X + 3,Y + 33 } };
	float hightlight1[][2] = { { X + 1, Y + 71},{X + 6,Y + 71},{X + 6, Y + 76},{X + 1,Y + 76} };
	float hightlight4[][2] = { { X + 1, Y + 50 },{ X + 6,Y + 50 },{ X + 6, Y + 55 },{ X + 1,Y + 55 } };
	float hightlight5[][2] = { { X + 1, Y + 43 },{ X + 6,Y + 43 },{ X + 6, Y + 48 },{ X + 1,Y + 48 } };
	float hightlight6[][2] = { { X + 1, Y + 36 },{ X + 6,Y + 36 },{ X + 6, Y + 41 },{ X + 1,Y + 41 } };
	utilities.setColor(0x006D651E);
	utilities.draw(GL_QUADS, box, 4, 5);
	utilities.setColor(0x00FFFFFF);
	switch (activeEditing)
	{
	case TEX_CROP:
		utilities.draw(GL_LINE_LOOP, hightlight1, 4, 2);
		break;
	case TEX_BRIGHT:
		utilities.draw(GL_LINE_LOOP, hightlight4, 4, 2);
		break;
	case TEX_CONTRAST:
		utilities.draw(GL_LINE_LOOP, hightlight5, 4, 2);
		break;
	case TEX_SATURATION:
		utilities.draw(GL_LINE_LOOP, hightlight6, 4, 2);
		break;
	default:
		break;
	}
	textures->DrawTexture(TEX_FOPEN, X + 1, Y + 85, X + 6, Y + 90);
	textures->DrawTexture(TEX_FSAVE, X + 1, Y + 78, X + 6, Y + 83);
	textures->DrawTexture(TEX_CROP, X + 1, Y + 71, X + 6, Y + 76);
	textures->DrawTexture(TEX_LROTATE, X + 1, Y + 64, X + 6, Y + 69);
	textures->DrawTexture(TEX_RROTATE, X + 1, Y + 57, X + 6, Y + 62);
	textures->DrawTexture(TEX_BRIGHT, X + 1, Y + 50, X + 6, Y + 55);
	textures->DrawTexture(TEX_CONTRAST, X + 1, Y + 43, X + 6, Y + 48);
	textures->DrawTexture(TEX_SATURATION, X + 1, Y + 36, X + 6, Y + 41);
	utilities.draw(GL_LINE_LOOP, plus, 13, 1);
	utilities.setColor(0x000022FF);
	utilities.draw(GL_QUADS, phfill, 4, 1);
	utilities.draw(GL_QUADS, pvfill, 4, 1);
	float minus[][2] = { { X + 2,Y + 18 },{ X + 5,Y + 18 },{ X + 5,Y + 20 },{ X + 2,Y + 20 } };
	float valueBox[][2] = { { X + 1,Y + 23 },{ X + 6,Y + 23 },{ X + 6,Y + 27 },{ X + 1,Y + 27 } };
	utilities.setColor(0x0088FF00);
	utilities.draw(GL_QUADS, minus, 4, 1);
	utilities.setColor(0x00DBE3E4);
	utilities.draw(GL_QUADS, valueBox, 4, 1);
	
	float originalSizeBtn[][2] = { { X + 1,Y + 10 },{ X + 6,Y + 10 },{ X + 6,Y + 14 },{ X + 1,Y + 14 } };
	float stretchSizeBtn[][2] = { { X + 1,Y + 4 },{ X + 6,Y + 4 },{ X + 6,Y + 8 },{ X + 1,Y + 8 } };
	if (fit)
	{
		utilities.setColor(0x0000FF00);
		utilities.draw(GL_QUADS, originalSizeBtn, 4, 5);
		utilities.setColor(0x00FFFFFF);
		utilities.draw(GL_QUADS, stretchSizeBtn, 4, 5);
	}
	else
	{
		utilities.setColor(0x00FFFFFF);
		utilities.draw(GL_QUADS, originalSizeBtn, 4, 5);
		utilities.setColor(0x0000FF00);
		utilities.draw(GL_QUADS, stretchSizeBtn, 4, 5);
	}
}

//Function to draw the right toolbar
void drawRToolBar(float X,float Y)
{
	//X=93,Y=0
	float box[][2] = { { X,Y },{ X + 7,Y },{ X + 7,93 },{X,93 } };
	
	utilities.setColor(0x0032312A);
	utilities.draw(GL_QUADS, box, 4, 5);
	utilities.setColor(0x00FFFFFF);
	textures->DrawTexture(TEX_IMG_NEGATIVE, X + 1, Y + 85, X + 6, Y + 90);
	textures->DrawTexture(TEX_IMG_SEPIA, X + 1, Y + 78, X + 6, Y + 83);
	textures->DrawTexture(TEX_IMG_GRAYSCALE, X + 1, Y + 71, X + 6, Y + 76);
	textures->DrawTexture(TEX_IMG_WARMSUN, X + 1, Y + 62, X + 6, Y + 69);
	textures->DrawTexture(TEX_IMG_COOLSHADE, X + 1, Y + 53, X + 6, Y + 60);
	textures->DrawTexture(TEX_IMG_IRRSATURATE, X + 1, Y + 44, X + 6, Y + 51);
	textures->DrawTexture(TEX_IMG_BLUR, X + 1, Y + 33, X + 6, Y + 41);
}

//Function to draw the titlebar
void drawTitleBar(float X, float Y)
{
	//glColor3f(1, 0, 0);
	utilities.setColor(0x00348259);
	float box[][2] = { { X,Y },{ 100,Y },{ 100,100 },{ X,100 } };
	utilities.draw(GL_QUADS, box, 4, 5);
	utilities.setColor(0x0031322A);
	float reset[][2] = { {X + 1,Y + 1},{X + 6,Y + 1},{X + 7,Y + 2},{X + 7,Y + 5},{X+6,Y+6},{X + 1,Y + 6} };//X: 1-7 Y:1-6
	utilities.draw(GL_POLYGON, reset, 6, 1);
	textures->DrawTexture(TEX_LOGO, 95, 94, 99,99);
}

//Function to draw the image canvas
void drawImageBox()
{
	float box[][2] = { {0,0},{100,0},{100,100},{0,100} };
	utilities.setColor(0x00BEBABC);
	utilities.draw(GL_QUADS, box, 4, 5);
}