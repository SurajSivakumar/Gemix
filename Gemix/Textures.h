/*
		Raw image texture loading code in Texures::LoadTexture() was taken
		from the OpenGL Texture Tutorial on wikibooks.org
		Link: https://en.wikibooks.org/wiki/OpenGL_Programming/Intermediate/Textures
*/
#ifndef __TEXTURES_H_INCLUDED
#define __TEXTURES_H_INCLUDED



#include<iostream>
#include<GL\freeglut.h>
#define TEX_LOGO 0 
#define TEX_FOPEN 1
#define TEX_FSAVE 2
#define TEX_LROTATE 4
#define TEX_RROTATE 5
#define TEX_CROP 3
#define TEX_BRIGHT 6
#define TEX_CONTRAST 7
#define TEX_SATURATION 8
#define TEX_IMG_NEGATIVE 9
#define TEX_IMG_SEPIA 10
#define TEX_IMG_GRAYSCALE 11
#define TEX_IMG_COOLSHADE 12
#define TEX_IMG_WARMSUN 13
#define TEX_IMG_SOLARIZE 14
#define TEX_IMG_IRRSATURATE 15
#define TEX_IMG_BLUR 16
using namespace std;
extern string ExecDir;		//current directory of .exe, for opening image files in /textures folder. Obtained from main.cpp

class Textures
{
							//_textures holds a number for each texture, _dLists holds the display list number corresponding to each texture.
	GLsizei _numOfTextures;								//needed for calling glDeleteTextures()
	
public:
	GLuint* _textures;
	Textures(int numOfTextures);
	void LoadTexture(string filePath, int width, int height, int texName);	//constructor. allocates "numOfTextures" elements in both arrays.
	void LoadTexture(char buffer[], int width, int height, int texName);			//Load raw image file
	void DrawTexture(int texName, int x1, int y1, int x2, int y2);						//calls display list for texture
	Textures::~Textures();								//Deletes display lists, textures and arrays.
};

#endif