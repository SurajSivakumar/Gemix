#pragma once
#include<Windows.h>
#define BTN_OPEN 0
#define BTN_SAVE 1
#define BTN_CROP 2
#define BTN_LROTATE 3
#define BTN_RROTATE	4
#define BTN_BRIGHT 5
#define BTN_CONTRAST 6
#define BTN_SATURATION 7
#define BTN_FIT 8
#define BTN_STRETCH 9
#define BTN_NEGATIVE 10
#define BTN_SEPIA 11
#define BTN_GRAYSCALE 12
#define BTN_ADD 13
#define BTN_MINUS 14
#define CROP_AREA 15
#define BTN_RESET 16
#define BTN_WARMSUN 17
#define BTN_COOLSHADE 18
#define BTN_IRRSATURATE 19
#define BTN_BLUR 20
class Util
{
public:
	HFONT hf;
	HWND OrigDC;
	LPWSTR wideString;
	COLORREF bColor;
	COLORREF fColor;
	int winX, winY;
	
	float coloR, coloG, coloB;
	Util();
	~Util();
	void Win32SetFont(HWND hwnd, const char* fName, long size, bool italics, bool antiAlias);							///<summary>Sets current active Win32 Font</summary>
	void Win32ThrowText(HWND hwnd,LPCSTR lpText, int x, int y, COLORREF Bakcolor, COLORREF ForeColor,int TextAlign);	///<summary>Writes text at desired location with options</summary>	
	void setColor(int hexColor);																						///<summary>Sets OpenGL color</summary>
	long map(long x, long in_min, long in_max, long out_min, long out_max);
	void draw(int GLLineType, float vertices[][2], int n, float width);													///<summary>Draws a GL Primitive</summary>
	bool buttonWithinBounds(int button, int x, int y);																	///<summary>Checks if x,y is within a particular button bounds</summary>
};

