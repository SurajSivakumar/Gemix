#include<Windows.h>
#include<iterator>
#include<algorithm>
#include<math.h>
#include"Util.h"
#include"UIDraw.h"
#include"Textures.h"
#include"ImageTranformations.h"
#include"resource1.h"

using namespace Gdiplus;
#pragma comment (lib,"Gdiplus.lib")

//External from UIDraw.h
extern int activeEditing;

//Globals
int activeMouseHover = -1;
string ExecDir;						///<summary>Holds path to current directory</summary>
ImageTransformations* transforms;	///<summary>Object for ImageTransfomations</summary>
Gdiplus::Graphics* g;				///<summary>General graphics object</summary>
Textures* tex;						///<summary>Object for Textures</summary>
Gdiplus::Image* image;				///<summary>Image GDI+</summary>
Gdiplus::Bitmap* bmp;				///<summary>Bitmap from (image) GDI+</summary>
HDC dc;								///<summary>Handle to device context (Window)</summary>
HWND window;						///<summary>Handle to window from dc</summary>
HWND console;						///<summary>Handle to the console window</summary>
wchar_t* fileName;
wchar_t* fname;
bool imageLoaded = false;
using namespace std;
int brightCounterVal = 0;			///<summary>Brightness counter</summary>
int contrastCounterVal = 0;			///<summary>Contrast counter</summary>
int saturateConterVal = 0;			///<summary>Saturation counter</summary>
int rotAngle = 0;					///<summary>Image rotation angle variable</summary>
Rect cropRect(-1,-1,-1,-1);			///<summary>Crop rectangle</summary>


//Glut Display Callback
void initializeComponents()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(1, 1, 1, 1);
	glColor3f(0, 0, 0);
	if(transforms!=NULL && transforms->fit)
	drawLToolBar(0, 0,true);
	else
		drawLToolBar(0, 0, false);
	drawRToolBar(93, 0);
	drawTitleBar(0, 93);
	glFlush();
	glViewport(0.08*utilities.winX, 0.015*utilities.winY, 0.92*utilities.winX - 0.08*utilities.winX, 0.90*utilities.winY);
	drawImageBox();
	glFlush();
	glViewport(0, 0, utilities.winX, utilities.winY);
	if (imageLoaded)
	{
		if (!transforms->fit)
		{
			transforms->x1 = 0.08*utilities.winX;
			transforms->y1 = 0.015*utilities.winY;
			transforms->x2 = 0.92*utilities.winX - 0.08*utilities.winX;
			transforms->y2 = 0.90*utilities.winY;
		}
		else
		{
			int width = transforms->width;
			int height = transforms->height;

			if (width >= height)
			{
				int newWidth = 0.92*utilities.winX - 0.08*utilities.winX;
				int newHeight = newWidth * (float)height / width;
				newHeight = (newHeight > 0.90*utilities.winY) ? 0.90*utilities.winY : newHeight;
				transforms->x1 = 0.08*utilities.winX;
				transforms->x2 = newWidth;
				transforms->y2 = newHeight;
				transforms->y1 = ((0.90*utilities.winY) / 2) - (newHeight / 2);
			}
			else
			{
				int newHeight = 0.90*utilities.winY;
				int newWidth = newHeight * ((float)width / height);
				transforms->x1 = ((0.92*utilities.winX - 0.08*utilities.winX)/2) - ((float)newWidth/2);
				transforms->x2 = newWidth;
				transforms->y2 = newHeight;
				transforms->y1 = 0.01*utilities.winX;;
			}
		}
		glViewport(transforms->x1, transforms->y1, transforms->x2, transforms->y2);
		glColor3f(1, 0, 0);
		transforms->drawImageTexture(rotAngle);
		glFlush();
		glViewport(0, 0, utilities.winX, utilities.winY);
	}
	if (activeEditing == TEX_CROP && cropRect.X != -1 && cropRect.Y != -1 && cropRect.Width != -1 && cropRect.Height != -1)
	{
		int boxX1, boxY1, boxX2, boxY2;
		boxX1 = utilities.map(cropRect.X, 0, utilities.winX, 0, 100);
		boxX2 = utilities.map(cropRect.Width, 0, utilities.winX, 0, 100);
		boxY1 = utilities.map(cropRect.Y, 0, utilities.winY, 0, 100);
		boxY2 = utilities.map(cropRect.Height, 0, utilities.winY, 0, 100);
		utilities.setColor(0x00000000);
		float cropBox[][2] = { { boxX1, 100-boxY1 },{ boxX2,100-boxY1 },{ boxX2,100-boxY2 },{ boxX1, 100-boxY2 } };
		utilities.draw(GL_LINE_LOOP, cropBox, 4, 2);
	}
	glutSwapBuffers();
	glFinish();
	COLORREF white = 0x00FFFFFF;
	COLORREF red = 0x000000FF;
	utilities.Win32SetFont(window,"Times New Roman", (0.06*utilities.winY), false, true);
	utilities.Win32ThrowText(window,"Gemix", 0.94*utilities.winX, (0.07 * utilities.winY) / 9, 0x00348259, white,TA_RIGHT);
	utilities.Win32SetFont(window,"Times New Roman", 0.04*utilities.winY, false, true);
	char buff[6];
	if (activeEditing == TEX_BRIGHT)
		itoa(brightCounterVal, buff, 10);
	else if (activeEditing == TEX_CONTRAST)
		itoa(contrastCounterVal, buff, 10);
	else if (activeEditing == TEX_SATURATION)
		itoa(saturateConterVal, buff, 10);
	else
		itoa(0, buff, 10);
	utilities.Win32ThrowText(window,buff, utilities.map(1, 0, 100, 0, utilities.winX),utilities.winY- utilities.map(27, 0, 100, 0, utilities.winY), 0x00DBE3E4, 0x00000000, TA_LEFT);
	utilities.Win32SetFont(window,"Times New Roman", 0.03*utilities.winY, false, true);
	utilities.Win32ThrowText(window,"Reset", utilities.map(2, 0, 100, 0, utilities.winX), utilities.winY - utilities.map(98, 0, 100, 0, utilities.winY), 0x0031322A, 0x00FFFFFF, TA_LEFT);
	utilities.Win32SetFont(window,"Times New Roman", 0.03*utilities.winY, false, true);
	if (transforms==NULL || (transforms!=NULL && !transforms->fit))
	{
		utilities.Win32ThrowText(window,"Fit", utilities.map(2, 0, 100, 0, utilities.winX), utilities.winY - utilities.map(13, 0, 100, 0, utilities.winY), 0x00FFFFFF, 0x00000000, TA_LEFT);
		utilities.Win32ThrowText(window,"Stch", utilities.map(2, 0, 100, 0, utilities.winX), utilities.winY - utilities.map(7, 0, 100, 0, utilities.winY), 0x0000FF00, 0x00000000, TA_LEFT);
	}
	if(transforms != NULL && transforms->fit)
	{
		utilities.Win32ThrowText(window,"Fit", utilities.map(2, 0, 100, 0, utilities.winX), utilities.winY - utilities.map(13, 0, 100, 0, utilities.winY), 0x0000FF00, 0x00000000, TA_LEFT);
		utilities.Win32ThrowText(window,"Stch", utilities.map(2, 0, 100, 0, utilities.winX), utilities.winY - utilities.map(7, 0, 100, 0, utilities.winY), 0x00FFFFFF, 0x00000000, TA_LEFT);
	}
	
	if (!imageLoaded)
	{
		utilities.Win32SetFont(window,"Times New Roman", 36, false, true);
		utilities.Win32ThrowText(window,"OpenGL+Win32 based simple image editor", utilities.winX / 2, utilities.winY*0.25, 0x00BEBABC, 0x00000000, TA_CENTER);
		utilities.Win32ThrowText(window,"Suraj S", utilities.winX / 2, utilities.winY*0.35, 0x00BEBABC, 0x00000000, TA_CENTER);
		utilities.Win32ThrowText(window,"Karthikeyen G", utilities.winX / 2, utilities.winY*0.42, 0x00BEBABC, 0x00000000, TA_CENTER);
		utilities.Win32ThrowText(window,"Load an Image...", utilities.winX / 2, utilities.winY *0.55, 0x00BEBABC, 0x00000000, TA_CENTER);
	}
}

//Glut Reshape Callback
void resize(int x, int y)
{
	if (x < 850)
	{
		utilities.winX =  850;
		glutReshapeWindow(850, y);
	}
	if (y < 600)
	{
		utilities.winY = 600;
		glutReshapeWindow(x, 600);
	}
	utilities.winX =  x;
	utilities.winY = y;
	glViewport(0, 0, x, y);
}

//Glut Mouse Callback
void mouseEvent(int MouseBtn, int BtnState, int x, int y)
{
	switch (MouseBtn)
	{
	case GLUT_LEFT_BUTTON:
		if (BtnState == GLUT_DOWN && activeEditing == TEX_CROP && utilities.buttonWithinBounds(CROP_AREA,x,y))
		{
			cropRect.X = x;
			cropRect.Y = y;
			cropRect.Width = -1;
			cropRect.Height = -1;
		}
		if (BtnState == GLUT_UP && activeEditing == TEX_CROP && utilities.buttonWithinBounds(CROP_AREA, x, y))
		{
			cropRect.Width =x;
			cropRect.Height = y;
			
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_OPEN, x, y))
		{
			
			LPSTR filebuff = new char[256];
			string message = "Selected file - ";
			OPENFILENAME open = { 0 };
			open.lStructSize = sizeof(OPENFILENAME);
			open.hwndOwner = window;
			open.lpstrFilter = "Image Files(.jpg|.png|.bmp|.jpeg)\0*.jpg;*.png;*.bmp;*.jpeg\0\0";
			open.lpstrCustomFilter = NULL;
			open.lpstrFile = filebuff;
			open.lpstrFile[0] = '\0';
			open.nMaxFile = 256;
			open.nFilterIndex = 1;
			open.lpstrInitialDir = NULL;
			open.lpstrTitle = "Select An Image File\0";
			open.nMaxFileTitle = strlen("Select an image file\0");
			open.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_EXPLORER;
			if (GetOpenFileName(&open))
			{
				delete image;
				delete bmp;
				image = NULL;
				bmp = NULL;
				delete transforms;
			    fname = new wchar_t[strlen(open.lpstrFile) + 1];
				mbstowcs(fname, open.lpstrFile, strlen(open.lpstrFile) + 1);
				wstring tempf(fname);
				size_t found = tempf.find_last_of(L"/\\");
				fileName = (wchar_t*)(tempf.substr(0, found)).c_str();
				image = Gdiplus::Image::FromFile(fname);
				bmp = new Bitmap(image->GetWidth(), image->GetHeight(), image->GetPixelFormat());
				Graphics* temp = new Graphics(bmp);
				temp->DrawImage(image, 0, 0, image->GetWidth(), image->GetHeight());
				transforms = new ImageTransformations(bmp, bmp->GetWidth(), bmp->GetHeight());
				imageLoaded = true;
				MessageBox(window, (message.append(open.lpstrFile)).c_str(), "information", MB_OK);
				brightCounterVal = 0;
				saturateConterVal = 0;
				contrastCounterVal = 0;
				glutPostRedisplay();
				delete temp;
			}
			delete[] filebuff;
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_SAVE, x, y))
		{
			ShowWindow(console, SW_SHOW);
			cout << "Enter a file Name (type x to exit): ";
			char newName[200];
			cin >> newName;
			if(strcmp(newName,"x")!=0)
			while (true)
			{
				string temp = ExecDir;
				temp.append("\\");
				temp.append(newName);
				WIN32_FIND_DATA FindFileData;
				HANDLE handle = FindFirstFile(newName, &FindFileData);
				int found = handle != INVALID_HANDLE_VALUE;
				if (found)
				{
					FindClose(handle);
					cout << "File exists! overwrite(y-yes,n-no) :";
					char ch;
					cin >> ch;
					if (ch == 'y' || ch == 'Y')
					{
						wchar_t* savef = new wchar_t[strlen(newName) + 1];
						mbstowcs(savef, newName, strlen(newName) + 1);
						if (transforms->saveFile(savef, rotAngle))
							MessageBox(window, "File saved!", "Information", MB_OK);
						else
							MessageBox(window, "File save failed!", "Information", MB_OK);
						break;
					}
					else
					{
						cout << "Enter a file Name (type x to exit):";
						cin >> newName;
					}
					if (strcmp(newName,"x")==0)
						break;
				}
				else
				{
					wchar_t* savef = new wchar_t[strlen(newName) + 1];
					mbstowcs(savef, newName, strlen(newName) + 1);
					if (transforms->saveFile(savef,rotAngle))
						MessageBox(window, "File saved!", "Information", MB_OK);
					else
						MessageBox(window, "File save failed!", "Information", MB_OK);
					break;
				}
			}
			ShowWindow(console, SW_HIDE);
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_CROP, x, y))
		{

			if (imageLoaded)
			{
				if (!transforms->fit)
					setActive(TEX_CROP);
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_RROTATE, x, y))
		{
			rotAngle += 90;
			if (rotAngle > 270)
				rotAngle = 0;
			glutPostRedisplay();
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_LROTATE, x, y))
		{
			rotAngle -= 90;
			if (rotAngle < 0)
				rotAngle = 270;
			glutPostRedisplay();
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_BRIGHT, x, y))
		{
			setActive(TEX_BRIGHT);
			glutPostRedisplay();
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_CONTRAST, x, y))
		{
			setActive(TEX_CONTRAST);
			glutPostRedisplay();
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_SATURATION, x, y))
		{
			setActive(TEX_SATURATION);
			glutPostRedisplay();
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_FIT, x, y))
		{

			if (imageLoaded)
			{
				if(activeEditing != TEX_CROP)
				transforms->fit = true;
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_STRETCH, x, y))
		{
			if (imageLoaded)
			{
				transforms->fit = false;
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_NEGATIVE, x, y))
		{
			if (imageLoaded)
			{
				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
				transforms->imageNegative(true);
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_GRAYSCALE, x, y))
		{
			if (imageLoaded)
			{
				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
				transforms->imageGrayScale(true);
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_SEPIA, x, y))
		{
			if (imageLoaded)
			{
				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
				transforms->imageSepia(true);
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_WARMSUN, x, y))
		{
			if (imageLoaded)
			{
				
				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
				transforms->warmSun(false);
				glutPostRedisplay();
			}
		}
	else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_COOLSHADE, x, y))
		{
		if (imageLoaded)
		{
			utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
			utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
			transforms->coolShade(false);
			glutPostRedisplay();
		}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_IRRSATURATE, x, y))
		{
			if (imageLoaded)
			{
				
				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
				transforms->irrSaturate(false);
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_BLUR, x, y))
		{
			if (imageLoaded)
			{

				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
				ShowWindow(console, SW_SHOW);
				cout << "Blur Mode\n";
				cout << "Enter the amount of blur (will be rounded to nearest odd number):";
				int blrAmt;
				while (true)
				{
					cin >> blrAmt;
					if (blrAmt < 3)
					{
						cout << "Enter a value >=3 :";
					}
					else
						break;
				}
				ShowWindow(console, SW_HIDE);
				if (blrAmt % 2 == 0)
					blrAmt++;
				transforms->blurImage(blrAmt);
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_ADD, x, y))
		{
			if (imageLoaded)
			{
				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
				if (activeEditing == TEX_BRIGHT)
				{
					brightCounterVal += 5;
					if (brightCounterVal > 50)
						brightCounterVal = 50;
					else
						transforms->chgBrightness(5, false);
				}
				if (activeEditing == TEX_CONTRAST)
				{
					contrastCounterVal += 5;
					if (contrastCounterVal > 100)
						contrastCounterVal = 100;
					else
						transforms->chgContrast(contrastCounterVal, true);
				}
				if (activeEditing == TEX_SATURATION)
				{
					saturateConterVal += 5;
					if (saturateConterVal > 100)
						saturateConterVal = 100;
					else
						transforms->chgSaturation(5);
				}
				glutPostRedisplay();
			}
		}

		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_MINUS, x, y))
		{
			if (imageLoaded)
			{
				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.42, 0x00ffffff, 0x00000000, TA_CENTER);
				if (activeEditing == TEX_BRIGHT)
				{
					brightCounterVal -= 5;
					if (brightCounterVal < -50)
						brightCounterVal = -50;
					else
						transforms->chgBrightness(-5, false);
				}
				if (activeEditing == TEX_CONTRAST)
				{
					contrastCounterVal -= 5;
					if (contrastCounterVal < -100)
						contrastCounterVal = -100;
					else
						transforms->chgContrast(contrastCounterVal, true);
				}
				if (activeEditing == TEX_SATURATION)
				{
					saturateConterVal -= 5;
					if (saturateConterVal < -100)
						saturateConterVal = -100;
					else
						transforms->chgSaturation(-5);

				}
				glutPostRedisplay();
			}
		}
		else if (BtnState == GLUT_UP && utilities.buttonWithinBounds(BTN_RESET, x, y))
		{
			if (imageLoaded)
			{
				utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
				utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.42, 0x00ffffff, 0x00000000, TA_CENTER);
				if (imageLoaded)
					transforms->resetToOriginal();
				brightCounterVal = contrastCounterVal = saturateConterVal = 0;
				glutPostRedisplay();
			}
		}
		break;
		case GLUT_RIGHT_BUTTON:
			if (BtnState == GLUT_UP)
			{
				if(activeEditing==TEX_CROP && cropRect.X !=-1 && cropRect.Y != -1 && cropRect.Width != -1 && cropRect.Height != -1 )
				transforms->cropImage(cropRect, utilities.winX, utilities.winY);
				cropRect.X = -1;
				cropRect.Y = -1;
				activeEditing = -1;
				glutPostRedisplay();
			}
			break;
	default:
		break;
	}
	
}

//Glut Motion Callback
void motionEvent(int x, int y)
{
	if (activeEditing == TEX_CROP && cropRect.X != -1 && cropRect.Y != -1)
	{
		cropRect.Width =x;
		cropRect.Height = y;
		glutPostRedisplay();
	}
}


//Glut Keyboard Callback
void keyEvent(unsigned char key, int x, int y)
{
	if (imageLoaded)
	{
		if (glutGetModifiers() == GLUT_ACTIVE_ALT && (key == 'z' || key == 'Z'))
		{
			if (imageLoaded)
			{
				transforms->undoChangesHandler();
				glutPostRedisplay();
			}
		}
		if (key == '+')
		{
			if (activeEditing == TEX_BRIGHT)
			{
				if (imageLoaded)
				{
					utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
					utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
					brightCounterVal += 5;
					if (brightCounterVal > 50)
						brightCounterVal = 50;
					else
						transforms->chgBrightness(5, false);
				}
			}
			if (activeEditing == TEX_CONTRAST)
			{
				if (imageLoaded)
				{
					utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
					utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
					contrastCounterVal += 5;
					if (contrastCounterVal > 100)
						contrastCounterVal = 100;
					else
						transforms->chgContrast(contrastCounterVal, true);
				}
			}
			if (activeEditing == TEX_SATURATION)
			{
				if (imageLoaded)
				{
					utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
					utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
					saturateConterVal += 5;
					if (saturateConterVal > 100)
						saturateConterVal = 100;
					else
						transforms->chgSaturation(5);
				}
			}
			glutPostRedisplay();
		}
		if (key == '-')
		{
			if (activeEditing == TEX_BRIGHT)
			{
				if (imageLoaded)
				{
					utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
					utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
					brightCounterVal -= 5;
					if (brightCounterVal < -50)
						brightCounterVal = -50;
					else
						transforms->chgBrightness(-5, false);
				}
				
			}
			if (activeEditing == TEX_CONTRAST)
			{
				if (imageLoaded)
				{
					utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
					utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
					contrastCounterVal -= 5;
					if (contrastCounterVal < -100)
						contrastCounterVal = -100;
					else
						transforms->chgContrast(contrastCounterVal, true);
				}
			}
			if (activeEditing == TEX_SATURATION)
			{
				if (imageLoaded)
				{
					utilities.Win32SetFont(window, "Times New Roman", 36, false, true);
					utilities.Win32ThrowText(window, "Processing...", utilities.winX / 2, utilities.winY*0.5, 0x00ffffff, 0x00000000, TA_CENTER);
					saturateConterVal -= 5;
					if (saturateConterVal < -100)
						saturateConterVal = -100;
					else
						transforms->chgSaturation(-5);
				}
			}
			glutPostRedisplay();
		}
	}
}

//Main function
int main(int argc, char *argv[])
{
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	ExecDir = argv[0];
	size_t found = ExecDir.find_last_of("/\\");
	ExecDir = ExecDir.substr(0, found);
	wstring wExecDir;
	HICON icon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(glut_icon));
	glutInit(&argc, argv);
	
	//Initial window parameters
	glutInitWindowSize(850, 600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	
	//Creation of Windows
	glutCreateWindow("Gemix");
	dc = wglGetCurrentDC();
	g = new Graphics(dc);
	window = WindowFromDC(dc);
	SendMessage(window, WM_SETICON, ICON_BIG, (LPARAM)icon);
	glutDisplayFunc(initializeComponents);
	
	//Hide the console Window
	console = GetConsoleWindow();
	ShowWindow(console, SW_HIDE);
	HMENU hmenu = GetSystemMenu(console, FALSE);
	EnableMenuItem(hmenu, SC_CLOSE, MF_GRAYED);
	//Set XY max coordinates
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	
	//Window Resize Function
	glutReshapeFunc(resize);
	glutMouseFunc(mouseEvent);
	glutKeyboardFunc(keyEvent);
	glutMotionFunc(motionEvent);

	
	//Glut Main Loop
	 tex = new Textures(17);
	 loadRawsToMemory(tex,ExecDir);
	glutMainLoop();
	GdiplusShutdown(gdiplusToken);

	return 0;
}
