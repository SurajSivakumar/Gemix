#include "Util.h"
#include<GL\freeglut.h>

Util::Util()
{
	hf = NULL;
	OrigDC = NULL;
}


Util::~Util()
{
}

void Util::Win32SetFont(HWND hwnd,const char* fName, long size, bool italics, bool antiAlias)
{
	long lfHeight;
	HDC hdc;
	if (OrigDC == NULL)
		OrigDC = hwnd;
	if (OrigDC == hwnd)
	{
		hdc = GetDC(hwnd);
		if (size == NULL)
			lfHeight = -MulDiv(12, GetDeviceCaps(hdc, LOGPIXELSY), 72);
		else
			lfHeight = size;
		if (antiAlias == TRUE)
			hf = CreateFont(lfHeight, 0, 0, 0, 0, italics, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, fName);
		else
			hf = CreateFont(lfHeight, 0, 0, 0, 0, italics, 0, 0, 0, 0, 0, 0, 0, fName);
		ReleaseDC(hwnd, hdc);
	}
}
bool Util::buttonWithinBounds(int button, int x, int y)
{
	switch (button)
	{
	case BTN_OPEN:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) < 6 && (100 -  map(y, 0,  winY, 0, 100)) > 85 && (100 -  map(y, 0,  winY, 0, 100)) <= 90)
			return true;
		else
			return false;
	case BTN_SAVE:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) <6 && (100 -  map(y, 0,  winY, 0, 100)) >78 && (100 -  map(y, 0,  winY, 0, 100)) <= 83)
			return true;
		else
			return false;
	case BTN_CROP:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) <6 && (100 -  map(y, 0,  winY, 0, 100)) >71 && (100 -  map(y, 0,  winY, 0, 100)) <= 76)
			return true;
		else
			return false;
	case BTN_LROTATE:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) <6 && (100 -  map(y, 0,  winY, 0, 100)) >64 && (100 -  map(y, 0,  winY, 0, 100)) <= 69)
			return true;
		else
			return false;
	case BTN_RROTATE:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) <6 && (100 -  map(y, 0,  winY, 0, 100)) >57 && (100 -  map(y, 0,  winY, 0, 100)) <= 62)
			return true;
		else
			return false;
	case BTN_BRIGHT:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) <6 && (100 -  map(y, 0,  winY, 0, 100)) >50 && (100 -  map(y, 0,  winY, 0, 100)) <= 55)
			return true;
		else
			return false;
	case BTN_CONTRAST:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) < 6 && (100 -  map(y, 0,  winY, 0, 100)) > 43 && (100 -  map(y, 0,  winY, 0, 100)) <= 48)
			return true;
		else
			return false;
	case BTN_SATURATION:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) <6 && (100 -  map(y, 0,  winY, 0, 100)) >36 && (100 -  map(y, 0,  winY, 0, 100)) <= 41)
			return true;
		else
			return false;
	case BTN_FIT:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) <6 && (100 -  map(y, 0,  winY, 0, 100)) >10 && (100 -  map(y, 0,  winY, 0, 100)) <= 14)
			return true;
		else
			return false;
	case BTN_STRETCH:
		if ( map(x, 0,  winX, 0, 100) >= 1 &&  map(x, 0,  winX, 0, 100) <6 && (100 -  map(y, 0,  winY, 0, 100)) >4 && (100 -  map(y, 0,  winY, 0, 100)) <= 8)
			return true;
		else
			return false;
	case BTN_NEGATIVE:
		if ( map(x, 0,  winX, 0, 100) >= 94 &&  map(x, 0,  winX, 0, 100) <99 && (100 -  map(y, 0,  winY, 0, 100)) >85 && (100 -  map(y, 0,  winY, 0, 100)) <= 90)
			return true;
		else
			return false;
	case BTN_SEPIA:
		if ( map(x, 0,  winX, 0, 100) >= 94 &&  map(x, 0,  winX, 0, 100) <99 && (100 -  map(y, 0,  winY, 0, 100)) >78 && (100 -  map(y, 0,  winY, 0, 100)) <= 83)
			return true;
		else
			return false;
	case BTN_GRAYSCALE:
		if ( map(x, 0,  winX, 0, 100) >= 94 &&  map(x, 0,  winX, 0, 100) <99 && (100 -  map(y, 0,  winY, 0, 100)) >71 && (100 -  map(y, 0,  winY, 0, 100)) <= 76)
			return true;
		else
			return false;
	case BTN_WARMSUN:
		if (map(x, 0, winX, 0, 100) >= 94 && map(x, 0, winX, 0, 100) <99 && (100 - map(y, 0, winY, 0, 100)) >62 && (100 - map(y, 0, winY, 0, 100)) <= 69)
			return true;
		else
			return false;
	case BTN_COOLSHADE:
		if (map(x, 0, winX, 0, 100) >= 94 && map(x, 0, winX, 0, 100) <99 && (100 - map(y, 0, winY, 0, 100)) >53 && (100 - map(y, 0, winY, 0, 100)) <= 60)
			return true;
		else
			return false;
	case BTN_IRRSATURATE:
		if (map(x, 0, winX, 0, 100) >= 94 && map(x, 0, winX, 0, 100) <99 && (100 - map(y, 0, winY, 0, 100)) >44 && (100 - map(y, 0, winY, 0, 100)) <= 51)
			return true;
		else
			return false;
	case BTN_BLUR:
		if (map(x, 0, winX, 0, 100) >= 94 && map(x, 0, winX, 0, 100) <99 && (100 - map(y, 0, winY, 0, 100)) >33 && (100 - map(y, 0, winY, 0, 100)) <= 41)
			return true;
		else
			return false;
	case BTN_ADD:
		if (map(x, 0, winX, 0, 100) >= 2 && map(x, 0, winX, 0, 100) <5 && (100 - map(y, 0, winY, 0, 100)) >30 && (100 - map(y, 0, winY, 0, 100)) <= 33)
			return true;
		else
			return false;
	case BTN_MINUS:
		if (map(x, 0, winX, 0, 100) >= 2 && map(x, 0, winX, 0, 100) <5 && (100 - map(y, 0, winY, 0, 100)) >18 && (100 - map(y, 0, winY, 0, 100)) <= 20)
			return true;
		else
			return false;
	case CROP_AREA:
		if ( x >= 0.08*winX && x <0.92*winX && 0.015*winY <=y && 0.985*winY > y)
			return true;
		else
			return false;
	case BTN_RESET:
		if (map(x, 0, winX, 0, 100) >= 1 && map(x, 0, winX, 0, 100) <7 && (100 - map(y, 0, winY, 0, 100)) >94 && (100 - map(y, 0, winY, 0, 100)) <= 99)
			return true;
		else
			return false;
	default:
		return false;
	}
}

void Util::Win32ThrowText(HWND hwnd,LPCSTR lpText, int x, int y, COLORREF Bakcolor, COLORREF ForeColor,int TextAlign)
{
	HDC hdc;
	if (OrigDC == NULL)
		OrigDC = hwnd;
	if (OrigDC == hwnd)
	{
		hdc = GetDC(hwnd);
		SetTextAlign(hdc, TextAlign);
		SetBkColor(hdc, Bakcolor);
		SetTextColor(hdc, ForeColor);
		SelectObject(hdc, hf);
		TextOut(hdc, x, y, lpText, lstrlen(lpText));
		ReleaseDC(hwnd, hdc);
	}
}

void Util::draw(int GLShapeType, float vertices[][2], int n, float size)
{
	glLineWidth(size);
	glBegin(GLShapeType);

	for (int i = 0; i<n; i++)
		glVertex2f(vertices[i][0], vertices[i][1]);

	glEnd();
	glLineWidth(1.0f);
}

long Util::map(long x, long in_min, long in_max, long out_min, long out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
void Util::setColor(int hexColor)
{
	COLORREF ref = hexColor;
	bColor = hexColor;
	coloR = GetRValue(ref)/255.0f;
	coloG = GetGValue(ref)/255.0f;
	coloB = GetBValue(ref)/255.0f;
	glColor3f(coloR, coloG, coloB);
}
