#include "ImageTranformations.h"
#include<iostream>
#include<Windows.h>
#include<random>
using namespace Gdiplus;
ImageProps* props;
Util utilGlobal;
float hsl[3];
int bgr[3];
ImageTransformations::ImageTransformations()
{
	pixelData = NULL;
}
int ImageTransformations::GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes
	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}
void ImageTransformations::getHSL(const int b,const int g,const int r)
{
	float nr=(float)r/255, ng=(float)g/255, nb = (float)b / 255;
	float Cmax = max(nr, ng); Cmax = max(Cmax, nb);
	float Cmin = min(nr, ng); Cmin = min(Cmin, nb);
	float h, s, l = ((float)Cmax + Cmin) / 2;
	if (Cmax == Cmin)
		h = s = 0;
	else
	{
		float d = Cmax - Cmin;
		s = (l > 0.5) ? (d / (2 - Cmax - Cmin)) : (d / (Cmax + Cmin));
		if (Cmax == nr)
			h = (ng - nb) / d + (ng < nb ? 6 : 0);
		else if(Cmax== ng)
			h = (nb - nr) / d + 2;
		else if(Cmax == nb)
			h = (nr - ng) / d + 4;
		h /= 6;
	}
	hsl[0] = h*360;
	hsl[1] = s;
	hsl[2] = l;
}

void ImageTransformations::getRGB(float h, float s, float l)
{
	float r, g, b;
	if (s == 0)
		r = g = b = l;
	float q = l < 0.5 ? l * (1 + s) : l + s - l * s;
	float p = 2 * l - q;
	r = hue2rgb(p, q, h + 1.0 / 3.0);
	g = hue2rgb(p, q, h);
	b = hue2rgb(p, q, h - 1.0 / 3.0);
	r *= 255;
	g *= 255;
	b *= 255;
	bgr[0] = b;
	bgr[1] = g;
	bgr[2] = r;
}

float ImageTransformations::hue2rgb(float p, float q, float t)
{
	if (t < 0) t += 1;
	if (t > 1) t -= 1;
	if (t < 1.0 / 6.0) return p + (q - p) * 6 * t;
	if (t < 1.0 / 2.0) return q;
	if (t < 2.0 / 3.0) return p + (q - p) * (2.0 / 3.0 - t) * 6;
	return p;
}

float ImageTransformations::map(float x, float in_min, float in_max, float out_min, float out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
Color ImageTransformations::getPix(long x, long y)
{
	try {
		long ind = x*stride + ((alpha) ? (y * 4) : (y * 3));
		byte b = pixelData[ind + 0];
		byte g = pixelData[ind + 1];
		byte r = pixelData[ind + 2];
		return Color(r, g, b);
	}
	catch (std::exception& e)
	{
		throw;
	}
}
bool ImageTransformations::setPix(long x, long y,Color set)
{
	long ind = x*stride + ((alpha) ? (y * 4) : (y * 3));
	pixelData[ind + 0] = set.GetB();
	pixelData[ind + 1] = set.GetG();
	pixelData[ind + 2] = set.GetR();
	return true;
}
bool ImageTransformations::refreshImageBuffer()
{
	try
	{
		glBindTexture(GL_TEXTURE_2D, imageTexture);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		int px = oBitmap->GetPixelFormat();
		if(px == PixelFormat16bppRGB555 || px== PixelFormat16bppRGB565 || px== PixelFormat24bppRGB || px == PixelFormat32bppRGB || px == PixelFormat48bppRGB)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width,height,
			0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pixelData);
		else
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, oBitmap->GetWidth(), oBitmap->GetHeight(),
				0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, pixelData);
		return true;
	}
	catch (const std::exception& e)
	{
		MessageBox(NULL, e.what(), "Error", MB_OK | MB_ICONERROR);
		return false;
	}
}

void ImageTransformations::drawImageTexture(int dir)
{
	
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, imageTexture);
	glBegin(GL_POLYGON);
	switch (dir)
	{
	case 0:
		glTexCoord2d(0, 0); glVertex2f(0, 100);
		glTexCoord2d(1, 0); glVertex2f(100, 100);
		glTexCoord2d(1, 1); glVertex2f(100, 0);
		glTexCoord2d(0, 1); glVertex2f(0, 0);
		/*gl.glTexCoord2d(0, 1);
		gl.glVertex2d(left, top);
		gl.glTexCoord2d(1, 1);
		gl.glVertex2d(left + width, top);
		gl.glTexCoord2d(1, 0);
		gl.glVertex2d(left + width, top + height);
		gl.glTexCoord2d(0, 0);
		gl.glVertex2d(left, top + height);*/
		break;
	case 90:
		glTexCoord2d(0, 0); glVertex2f(100, 100);
		glTexCoord2d(1, 0); glVertex2f(100, 0); 
		glTexCoord2d(1, 1); glVertex2f(0, 0);
		glTexCoord2d(0, 1); glVertex2f(0, 100);		
		break;
	case 180:
		glTexCoord2d(0, 0); glVertex2f(100, 0);
		glTexCoord2d(1, 0); glVertex2f(0, 0);
		glTexCoord2d(1, 1); glVertex2f(0, 100);
		glTexCoord2d(0, 1); glVertex2f(100, 100);
		break;
	case 270:
		glTexCoord2d(0, 0); glVertex2f(0, 0); 
		glTexCoord2d(1, 0); glVertex2f(0, 100);
		glTexCoord2d(1, 1);   glVertex2f(100, 100);
		glTexCoord2d(0, 1); glVertex2f(100, 0);
		break;
	}
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
}
void ImageTransformations::undoChangesHandler()
{
	if(props->list !=NULL)
	if (pixelData != NULL)
	{
		delete[] pixelData;
		Gdiplus::BitmapData* bitmapData = new Gdiplus::BitmapData();
		Gdiplus::Rect rect(0, 0, oBitmap->GetWidth(), oBitmap->GetHeight());
		if (Gdiplus::Ok == oBitmap->LockBits(
			&rect,
			Gdiplus::ImageLockModeRead | Gdiplus::ImageLockModeWrite,
			oBitmap->GetPixelFormat(),
			bitmapData
		))
		{
			int len = bitmapData->Height * (bitmapData->Stride);
			pixelData = new byte[len];
			memcpy(pixelData, bitmapData->Scan0, len);
			stride = bitmapData->Stride;
			oBitmap->UnlockBits(bitmapData);
		}
		TransformList* temp = props->list;
		TransformList* safe;
		if(temp->next !=nullptr)
		{
			while (temp->next != NULL)
			{
				if (temp->next == NULL)
					break;
				else
				{
					int thash = temp->thash;
					int value = temp->value;
					if (thash == TRANSFORM_NEGATIVE)
						imageNegative(false);
					else if (thash == TRANSFORM_SEPIA)
						imageSepia(false);
					else if (thash == TRANSFORM_GRAYSCALE)
						imageGrayScale(false);
				}
				temp = temp->next;
			}
			temp = props->list;
			while (temp->next->next != NULL)
				temp = temp->next;
			delete temp->next;
			temp->next = NULL;
		}
		else
		{
			delete props->list;
			props->list = NULL;
		}
		refreshImageBuffer();
	}
}
bool ImageTransformations::saveFile(wchar_t * fname, int rotAngle)
{
	if (alpha)
	{
		sBitmap = new Bitmap(width, height, stride, PixelFormat32bppARGB, pixelData);
		CLSID id;
		Status stat;
		switch (rotAngle)
		{
		case 90:
			sBitmap->RotateFlip(Rotate90FlipNone);
			break;
		case 180:
			sBitmap->RotateFlip(Rotate180FlipNone);
			break;
		case 270:
			sBitmap->RotateFlip(Rotate270FlipNone);
			break;
		}
		GetEncoderClsid(L"image/png", &id);
		stat = sBitmap->Save(fname, &id);
		delete[] sBitmap;
		if (stat == Gdiplus::Status::Ok)
			return true;
		else
			return false;
	}
	else
	{
		sBitmap = new Bitmap(width, height, stride, PixelFormat24bppRGB, pixelData);
		CLSID id;
		Status stat;
		GetEncoderClsid(L"image/jpeg", &id);
		switch (rotAngle)
		{
		case 90:
			sBitmap->RotateFlip(Rotate90FlipNone);
			break;
		case 180:
			sBitmap->RotateFlip(Rotate180FlipNone);
			break;
		case 270:
			sBitmap->RotateFlip(Rotate270FlipNone);
			break;
		}
		stat = sBitmap->Save(fname, &id, NULL);
		delete[] sBitmap;
		if (stat == Gdiplus::Status::Ok)
			return true;
		else
			return false;
	}
}
void ImageTransformations::chgContrast(int val)
{

	float factor = (259.0 * (val + 255.0)) / (255.0 * (259.0 - val));
		int len = height * (stride);
		long ind = 0;
		while (ind < len - 4)
		{
				hsl[0] = hslData[ind + 0];
				hsl[1] = hslData[ind + 1];
				hsl[2] = hslData[ind + 2];
				hsl[2] = (hsl[2] > 1.0) ? 1.0 : hsl[2];
				hsl[2] = (hsl[2] < 0.0) ? 0.0 : hsl[2];
				hsl[1] = (hsl[1] > 1.0) ? 1.0 : hsl[1];
				hsl[1] = (hsl[1] < 0.0) ? 0.0 : hsl[1];
				getRGB(hsl[0]/360.0, hsl[1], hsl[2]);		
				bgr[2] = (int)((float)factor * (bgr[2] - 128)) + 128;
				bgr[1] = (int)((float)factor * (bgr[1] - 128)) + 128;
				bgr[0] = (int)((float)factor * (bgr[0] - 128)) + 128;
				bgr[0] = (bgr[0] > 255) ? 255: bgr[0];
				bgr[1] = (bgr[1] > 255) ? 255 : bgr[1];
				bgr[2] = (bgr[2] > 255) ? 255 : bgr[2];
				bgr[0] = (bgr[0] < 0) ? 0 : bgr[0];
				bgr[1] = (bgr[1] < 0) ? 0 : bgr[1];
				bgr[2] = (bgr[2] < 0) ? 0 : bgr[2];
				pixelData[ind + 0] = (byte)bgr[0];
				pixelData[ind + 1] = (byte)bgr[1];
				pixelData[ind + 2] = (byte)bgr[2];
				if(alpha)
				ind += 4;
				else
				ind += 3;
			
		}

	refreshImageBuffer();

}

void ImageTransformations::chgSaturation(int val)
{
	long len = height * stride;
	long ind = 0;
	while (ind < len - 4)
	{
			hsl[0] = hslData[ind + 0];
			hsl[1] = hslData[ind + 1];
			hsl[2] = hslData[ind + 2];
			hsl[2] = (hsl[2] > 1.0) ? 1.0 : hsl[2];
			hsl[2] = (hsl[2] < 0.0) ? 0.0 : hsl[2];
			if (val > 0)
				hsl[1] = hsl[1] + 0.05*hsl[1];
			else if (val<0)
				hsl[1] = ((hsl[1]) / 1.05);
			hslData[ind + 1] = hsl[1];
			hsl[1] = (hsl[1] > 1.0) ? 1.0 : hsl[1];
			hsl[1] = (hsl[1] < 0.0) ? 0.0 : hsl[1];	
			getRGB(hsl[0] / 360.0, hsl[1], hsl[2]);
			pixelData[ind + 0] = (byte)bgr[0];
			pixelData[ind + 1] = (byte)bgr[1];
			pixelData[ind + 2] = (byte)bgr[2];
			if(alpha)
			ind += 4;
			else
			ind += 3;
	}
	refreshImageBuffer();
}

void ImageTransformations::cropImage(Gdiplus::Rect cropRect,int winWidth,int winHeight)
{
	int x1 = min(utilGlobal.map(cropRect.X, 0, winWidth,0,width), utilGlobal.map(cropRect.Width, 0, winWidth, 0, width));
	int y1 = min(utilGlobal.map(cropRect.Y, 0, winHeight, 0, height), utilGlobal.map(cropRect.Height, 0, winHeight, 0, height));
	int x2 = max(utilGlobal.map(cropRect.X, 0, winWidth, 0, width), utilGlobal.map(cropRect.Width, 0, winWidth, 0, width));
	int y2 = max(utilGlobal.map(cropRect.Y, 0, winHeight, 0, height), utilGlobal.map(cropRect.Height, 0, winHeight, 0, height));
	
	if (alpha)
	{
			sBitmap = new Gdiplus::Bitmap(width, height, stride, PixelFormat32bppARGB, pixelData); 
		cropped = new Gdiplus::Bitmap(x2 - x1, y2 - y1, PixelFormat32bppARGB);
	}
	else
	{
		sBitmap = new Gdiplus::Bitmap(width, height, stride, PixelFormat24bppRGB, pixelData);
		cropped = new Gdiplus::Bitmap(x2 - x1, y2 - y1, PixelFormat24bppRGB);
	}
		Gdiplus::Graphics g(cropped);
		Rect r(0, 0, x2 - x1, y2 - y1);
		g.DrawImage(sBitmap,r,x1,y1,x2-x1,y2-y1, Gdiplus::Unit::UnitPixel);
		Gdiplus::BitmapData* bitmapData = new Gdiplus::BitmapData();
		Gdiplus::Rect rect(0, 0, cropped->GetWidth(), cropped->GetHeight());
		if (Gdiplus::Ok == cropped->LockBits(
			&rect,
			Gdiplus::ImageLockModeRead | Gdiplus::ImageLockModeWrite,
			cropped->GetPixelFormat(),
			bitmapData
		))
		{
			delete[] pixelData;
			stride = bitmapData->Stride;
			width = cropped->GetWidth();
			height = cropped->GetHeight();
			
			int len = bitmapData->Height * (bitmapData->Stride);
			pixelData = new byte[len];
			delete[] hslData;
			hslData = new float[len];
			memcpy(pixelData, bitmapData->Scan0, len);
			cropped->UnlockBits(bitmapData);
		}
		long ind = 0;
		if (!alpha)
			while (ind < bitmapData->Stride *bitmapData->Height - 4)
			{
				byte b = pixelData[ind + 0];
				byte g = pixelData[ind + 1];
				byte r = pixelData[ind + 2];
				getHSL((int)b, (int)g, (int)r);
				hslData[ind + 0] = (float)hsl[0];
				hslData[ind + 1] = (float)hsl[1];
				hslData[ind + 2] = (float)hsl[2];
				ind += 3;
			}
		else
		{
			while (ind < bitmapData->Stride *bitmapData->Height - 4)
			{
				byte b = pixelData[ind + 0];
				byte g = pixelData[ind + 1];
				byte r = pixelData[ind + 2];
				byte a = pixelData[ind + 3];
				getHSL((int)b, (int)g, (int)r);
				hslData[ind + 0] = (float)hsl[0];
				hslData[ind + 1] = (float)hsl[1];
				hslData[ind + 2] = (float)hsl[2];
				hslData[ind + 3] = a;
				ind += 4;
			}
		}
		delete bitmapData;
		delete sBitmap;
		sBitmap = NULL;
	refreshImageBuffer();
	delete cropped;
}

void ImageTransformations::resetToOriginal()
{
	alpha = false;
	delete props;
	props = new ImageProps();
	Gdiplus::BitmapData* bitmapData = new Gdiplus::BitmapData();
	Gdiplus::Rect rect(0, 0, oBitmap->GetWidth(), oBitmap->GetHeight());
	if (Gdiplus::Ok == oBitmap->LockBits(
		&rect,
		Gdiplus::ImageLockModeRead | Gdiplus::ImageLockModeWrite,
		oBitmap->GetPixelFormat(),
		bitmapData
	))
	{
		delete[] pixelData;
		delete[] hslData;
		int len = bitmapData->Height * bitmapData->Stride;
		pixelData = new byte[len];
		hslData = new float[len];
		memcpy(pixelData, bitmapData->Scan0, len);
		stride = bitmapData->Stride;
		oBitmap->UnlockBits(bitmapData);
	}
	this->width = oBitmap->GetWidth();
	this->height = oBitmap->GetHeight();
	long ind = 0;
	
		while (ind < bitmapData->Stride *bitmapData->Height - 4)
		{
			byte b = pixelData[ind + 0];
			byte g = pixelData[ind + 1];
			byte r = pixelData[ind + 2];
			getHSL((int)b, (int)g, (int)r);
			hslData[ind + 0] = (float)hsl[0];
			hslData[ind + 1] = (float)hsl[1];
			hslData[ind + 2] = (float)hsl[2];
			if (alpha)
			{
				hslData[ind + 3] = (float)pixelData[ind + 3];
				ind += 4;
			}
			else
			ind += 3;
		}
	refreshImageBuffer();
}

void ImageTransformations::warmSun(bool add)
{
	long len = height * stride;
	long ind = 0;
	std::default_random_engine gen;
	std::uniform_int_distribution<int> dist(40,45);
	while (ind < len - 4)
	{
		hsl[0] = hslData[ind + 0];
		hsl[1] = hslData[ind + 1];
		hsl[2] = hslData[ind + 2];
		hsl[0] = dist(gen);
		hsl[1] = hsl[1] + hsl[1]*0.2;
		hsl[2] = hsl[2] + hsl[2] * 0.2;
		hsl[1] = (hsl[1] > 1.0) ? 1.0 : hsl[1];
		hsl[1] = (hsl[1] < 0.0) ? 0.0 : hsl[1];
		hsl[2] = (hsl[2] > 1.0) ? 1.0 : hsl[2];
		hsl[2] = (hsl[2] < 0.0) ? 0.0 : hsl[2];
		getRGB(hsl[0] / 360.0, hsl[1], hsl[2]);
		pixelData[ind + 0] = (byte)bgr[0];
		pixelData[ind + 1] = (byte)bgr[1];
		pixelData[ind + 2] = (byte)bgr[2];
		if (alpha)
			ind += 4;
		else
			ind += 3;
	}
	refreshImageBuffer();
}

void ImageTransformations::blurImage(int val)
{
	try {
		long ind;
		bgr[0] = bgr[1] = bgr[2] = 0;
		Color cVal(0, 0, 0);
		//Process Rows
		for (long x = 0; x < height; x++)
		{
			for (long y = 0; y < width; y++)
			{
				bgr[0] = bgr[1] = bgr[2] = 0;
				int k = 0, i = y - val / 2;
				int count = 0;
				while (k < val)
				{
					if (i >= 0 && i <= width)
					{
						cVal = getPix(x, i);
						bgr[0] += cVal.GetB();
						bgr[1] += cVal.GetG();
						bgr[2] += cVal.GetR();
						count++;
					}
					i++;
					k++;
				}

				Color sum(bgr[2] / count, bgr[1] / count, bgr[0] / count);
				setPix(x, y, sum);
			}
		}
	}
	catch (std::exception& e)
	{

	}
	refreshImageBuffer();
}

void ImageTransformations::coolShade(bool add)
{
	long len = height * stride;
	long ind = 0;
	std::default_random_engine gen;
	std::uniform_int_distribution<int> dist(170, 175);
	while (ind < len - 4)
	{
		hsl[0] = hslData[ind + 0];
		hsl[1] = hslData[ind + 1];
		hsl[2] = hslData[ind + 2];
		hsl[0] = dist(gen);
		hsl[1] = hsl[1] - hsl[1] * 0.2;
		hsl[2] = hsl[2] - hsl[2] * 0.2;
		hsl[1] = (hsl[1] > 1.0) ? 1.0 : hsl[1];
		hsl[1] = (hsl[1] < 0.0) ? 0.0 : hsl[1];
		hsl[2] = (hsl[2] > 1.0) ? 1.0 : hsl[2];
		hsl[2] = (hsl[2] < 0.0) ? 0.0 : hsl[2];
		getRGB(hsl[0] / 360.0, hsl[1], hsl[2]);
		pixelData[ind + 0] = (byte)bgr[0];
		pixelData[ind + 1] = (byte)bgr[1];
		pixelData[ind + 2] = (byte)bgr[2];
		if (alpha)
			ind += 4;
		else
			ind += 3;
	}
	refreshImageBuffer();
}
ImageTransformations::ImageTransformations(Gdiplus::Bitmap* bitmap,int width,int height)
{
	fit = false;
	alpha = false;
	pixelData = NULL;
	sBitmap = NULL;
	props = new ImageProps();
	Gdiplus::BitmapData* bitmapData = new Gdiplus::BitmapData();
	Gdiplus::Rect rect(0, 0, bitmap->GetWidth(), bitmap->GetHeight());
	if (Gdiplus::Ok ==bitmap->LockBits(
		&rect,
		Gdiplus::ImageLockModeRead | Gdiplus::ImageLockModeWrite,
		bitmap->GetPixelFormat(),
		bitmapData
	))
	{
		int len = bitmapData->Height * (bitmapData->Stride);
		pixelData = new byte[len];
		hslData = new float[len];
		memcpy(pixelData, bitmapData->Scan0, len);
		stride = bitmapData->Stride;
		bitmap->UnlockBits(bitmapData);
	}
	int px = bitmap->GetPixelFormat();
	if (px == PixelFormat16bppRGB555 || px == PixelFormat16bppRGB565 || px == PixelFormat24bppRGB || px == PixelFormat32bppRGB || px == PixelFormat48bppRGB)
	{
		oBitmap = new Gdiplus::Bitmap(width, height, PixelFormat24bppRGB);
		alpha = false;
	}
	else
	{
		oBitmap = new Gdiplus::Bitmap(width, height, PixelFormat32bppARGB);
		alpha = true;
	}
	this->width = bitmap->GetWidth();
	this->height = bitmap->GetHeight();
	Gdiplus::Graphics temp(oBitmap);
	temp.DrawImage(bitmap, 0, 0, width, height);
	
	long ind = 0;
	if (!alpha)
		while (ind < bitmapData->Stride *bitmapData->Height - 4)
		{
			byte b = pixelData[ind + 0];
			byte g = pixelData[ind + 1];
			byte r = pixelData[ind + 2];
			getHSL((int)b, (int)g, (int)r);
			hslData[ind + 0] = (float)hsl[0];
			hslData[ind + 1] = (float)hsl[1];
			hslData[ind + 2] = (float)hsl[2];
			ind += 3;
		}
	else
	{
		while (ind < bitmapData->Stride *bitmapData->Height - 4)
		{
			byte b = pixelData[ind + 0];
			byte g = pixelData[ind + 1];
			byte r = pixelData[ind + 2];
			byte a = pixelData[ind + 3];
			getHSL((int)b, (int)g, (int)r);
			hslData[ind + 0] = (float)hsl[0];
			hslData[ind + 1] = (float)hsl[1];
			hslData[ind + 2] = (float)hsl[2];
			hslData[ind + 3] = a;
			ind += 4;
		}
	}
	
	glGenTextures(1, &imageTexture);
	refreshImageBuffer();
}

ImageTransformations::~ImageTransformations()
{
	delete[] pixelData;
	delete[] hslData;
	if(sBitmap!=NULL)
	delete sBitmap;
	delete oBitmap;
	delete props;
}

void ImageTransformations::irrSaturate(bool add)
{
	long len = height * stride;
	long ind = 0;
	std::default_random_engine gen;
	std::uniform_real_distribution<float> dist(0.1, 0.25);
	std::uniform_real_distribution<float> distl(-0.1, 0.1);
	while (ind < len - 4)
	{
		hsl[0] = hslData[ind + 0];
		hsl[1] = hslData[ind + 1];
		hsl[2] = hslData[ind + 2];
		hsl[1] = hsl[1] + (dist(gen)) ;
		hsl[2] = hsl[2] + distl(gen);
		hsl[1] = (hsl[1] > 1.0) ? 1.0 : hsl[1];
		hsl[1] = (hsl[1] < 0.0) ? 0.0 : hsl[1];
		hsl[2] = (hsl[2] > 1.0) ? 1.0 : hsl[2];
		hsl[2] = (hsl[2] < 0.0) ? 0.0 : hsl[2];
		getRGB(hsl[0] / 360.0, hsl[1], hsl[2]);
		pixelData[ind + 0] = (byte)bgr[0];
		pixelData[ind + 1] = (byte)bgr[1];
		pixelData[ind + 2] = (byte)bgr[2];
		if (alpha)
			ind += 4;
		else
			ind += 3;
	}
	refreshImageBuffer();
}

void ImageTransformations::chgBrightness(int val)
{
	
	long len = height * stride;
		long ind = 0;
		while (ind < len - 4)
		{
			
				hsl[0] = hslData[ind + 0];
				hsl[1] = hslData[ind + 1];
				hsl[2] = hslData[ind + 2];
				hsl[1] = (hsl[1] > 1.0) ? 1.0 : hsl[1];
				hsl[1] = (hsl[1] < 0.0) ? 0.0 : hsl[1];
				if (val > 0)
					hsl[2] = hsl[2] + 0.05*hsl[2];
				else if(val<0)
					hsl[2] = ((hsl[2]) / 1.05);
				hslData[ind + 2] = hsl[2];
				hsl[2] = (hsl[2] > 1.0) ? 1.0 : hsl[2];
				hsl[2] = (hsl[2] < 0.0) ? 0.0 : hsl[2];
				
				getRGB(hsl[0] / 360.0, hsl[1], hsl[2]);
				pixelData[ind + 0] = (byte)bgr[0];
				pixelData[ind + 1] = (byte)bgr[1];
				pixelData[ind + 2] = (byte)bgr[2];
				if(alpha)
				ind += 4;
				else
				ind += 3;
			
		}
	refreshImageBuffer();
}

void ImageTransformations::imageNegative(bool add)
{
	long ind = 0;
	while (ind < stride*height -4)
	{
		
			byte b = pixelData[ind + 0];
			byte g = pixelData[ind + 1];
			byte r = pixelData[ind + 2];		
			pixelData[ind + 0] = 255 - b;
			pixelData[ind + 1] = 255 - g;
			pixelData[ind + 2] = 255 - r;
			if(alpha)
			ind += 4;
			else
			ind+=3;
		
	}
	if(add)
	props->addTransform(TRANSFORM_NEGATIVE, NULL);
	refreshImageBuffer();
}

void ImageTransformations::imageGrayScale(bool add)
{
	long ind = 0;
	while (ind < stride*height - 4)
	{
		
			byte b = pixelData[ind + 0];
			byte g = pixelData[ind + 1];
			byte r = pixelData[ind + 2];
			byte a = pixelData[ind + 3];
			byte gray = (r + b + g) / 3;
			pixelData[ind + 0] = gray;
			pixelData[ind + 1] = gray;
			pixelData[ind + 2] = gray;
			pixelData[ind + 3] = a;
			if (alpha)
				ind += 4;
			else
				ind += 3;
	}
	if(add)
	props->addTransform(TRANSFORM_GRAYSCALE, NULL);
	refreshImageBuffer();
}

void ImageTransformations::imageSepia(bool add)
{
	long ind = 0;
	while (ind < stride*height - 4)
	{
	
		
			int b = pixelData[ind + 0];
			int g = pixelData[ind + 1];
			int r = pixelData[ind + 2];
			r = (r*0.393) + (0.769*g) + (0.189*b);
			g = (0.349 * r) + (0.686*g) + (b*0.168);
			b = (0.272*r) + (0.534*g) + (b*0.131);
			if (r > 255)
				r = 255;
			if (b > 255)
				b = 255;
			if (g > 255)
				g = 255;
			pixelData[ind + 0] = (byte)b;
			pixelData[ind + 1] = (byte)g;
			pixelData[ind + 2] = (byte)r;
			if(alpha)
				ind += 4;
			else
				ind += 3;	
	}
	if(add)
	props->addTransform(TRANSFORM_SEPIA, NULL);
	refreshImageBuffer();
}

ImageProps::ImageProps()
{
	list = NULL;
}

ImageProps::~ImageProps()
{
}

bool ImageProps::addTransform(int thash, int value)
{
	try
	{
		if (list == NULL)
		{
			list = new TransformList();
			list->thash = thash;
			list->value = value;
			list->next = NULL;
		}
		else
		{
			TransformList* temp = list;
			while (temp->next != NULL)
				temp = temp->next;
			if (temp->thash != thash && value==NULL)
			{
				temp->next = new TransformList();
				temp->next->thash = thash;
				temp->next->value = value;
				temp->next->next = NULL;
			}
			else if(temp->thash==thash && value!=temp->value)
			{
				temp->value = value;
			}
		}
		return true;
	}
	catch (const std::exception& e)
	{
		MessageBox(NULL, e.what(), "Error", MB_ICONERROR | MB_OK);
		return false;
	}
}

