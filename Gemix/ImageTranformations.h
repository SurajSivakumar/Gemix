#pragma once
#include<GL\freeglut.h>
#include <objidl.h>
#include <gdiplus.h>
#include"Util.h"
#define TRANSFORM_BIGHTNESS 0
#define TRANSFORM_CONTRAST 1
#define TRANSFORM_SATURATION 2
#define TRANSFORM_NEGATIVE 3
#define TRANSFORM_SEPIA 4
#define TRANSFORM_GRAYSCALE 5
struct TransformList		///<summary>Transform List structure implements stack</summary>
{
	int thash;
	int value;
	TransformList* next;
};
class ImageProps			///<summary>ImageProps class manipulates TransformList</summary>
{

public:
	TransformList* list;
	ImageProps();
	~ImageProps();
	bool addTransform(int thash, int value);
};

class ImageTransformations
{
	int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
	void getHSL(int r, int g, int b);
	void getRGB(float h, float s, float l);
	float hue2rgb(float p, float q, float t);
	float map(float x, float in_min, float in_max, float out_min, float out_max);
	Gdiplus::Color getPix(long x, long y);
	bool setPix(long x, long y,Gdiplus::Color set);
public:
	int width; ///<summary>Width of the current image</summary>
	int height; ///<summary>Height of the current image</summary>
	int x1,x2;	///<summary>X coordinates of the recangle to draw texture</summary>
	int y1,y2;	///<summary>Y coordinates of the recangle to draw texture</summary>
	byte* pixelData; ///<summary>Holds Pixel color data in ARGB format</summary>
	float* hslData;
	bool alpha;
	bool fit;
	long stride;
	Gdiplus::Bitmap* sBitmap; ///<summary>Pointer to the current editing bitmap instance</summary>
	Gdiplus::Bitmap* oBitmap;
	Gdiplus::Bitmap* cropped;
	int activeEditing;
	GLuint imageTexture; ///<summary>Pointer to hold instance of image GLUtilities</summary>
	ImageTransformations();
	void coolShade(bool add);
	ImageTransformations(Gdiplus::Bitmap* bitmap,int width, int height);	///<summary>Initialize new bitmap </summary>
	~ImageTransformations();
	void irrSaturate(bool add);
	void chgBrightness(int val);
	void imageNegative(bool add); ///<summary>Apply image negative transform</summary>
	void imageGrayScale(bool add);
	void imageSepia(bool add);
	bool refreshImageBuffer(); ///<summary>Reload the pixelBuffer with the new image transform</summary>
	void drawImageTexture(int dir);	///<summary>Draw the current image texture to the screen</summary>
	void undoChangesHandler();
	bool saveFile(wchar_t* fname,int rotAngle);
	void chgContrast(int val);
	void chgSaturation(int val);
	void cropImage(Gdiplus::Rect rect,int winWidth,int winHeight);
	void resetToOriginal();
	void warmSun(bool add);
	void blurImage(int val);///<summary>Implements Horizontal Gaussian Blur</summary>
};